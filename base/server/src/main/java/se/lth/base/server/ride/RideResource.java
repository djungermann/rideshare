package se.lth.base.server.ride;

import se.lth.base.server.Config;
import se.lth.base.server.city.City;
import se.lth.base.server.city.CityDataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.ErrorType;
import se.lth.base.server.passenger.Passenger;
import se.lth.base.server.user.Role;
import se.lth.base.server.user.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("ride")
public class RideResource {

    // Ride data access wraps all database calls relating to the ride table.
    // The database URL is statically available.
    private final RideDataAccess rideDao = new RideDataAccess(Config.instance().getDatabaseDriver());

    // Current user, it is set by AuthenticationFilter. Based on a database lookup of the users login token.
    private final User user;

    public RideResource(@Context ContainerRequestContext context) {
        this.user = (User) context.getProperty(User.class.getSimpleName());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public Ride addRide(Ride ride) {
        try {
            return rideDao.addRide(user.getId(), ride.getStartCity(), ride.getEndCity(), ride.getLatestArrivalTime(), ride.getCarId(), ride.getAvailableCapacity());
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.BAD_GATEWAY);
        }
    }

    @Path("{rideId}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public boolean deleteRide(@PathParam("rideId") int rideId) {
        try {
            Ride ride = rideDao.getRideFromRideId(rideId);
            if(rideDao.isLocked(rideId, ride.getLatestArrivalTime(), ride.getStartCity(), ride.getEndCity())){
                return false;
            }
            if (user.getRole().clearanceFor(Role.ADMIN)) {
                return rideDao.deleteRideAsAdmin(rideId);
            } else {
                return rideDao.deleteRideAsDriver(rideId, user.getId());
            }
        } catch (Exception e) {
            throw new WebApplicationException("The ride could not be deleted", Response.Status.BAD_REQUEST);
        }
    }

    @Path("driver/{driverId}")
    @GET
    @RolesAllowed(Role.Names.ADMIN)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getDriverRides(@PathParam("driverId") int driverId) {
        try {
            return rideDao.getRidesWhereDriver(driverId);
        } catch (Exception e) {
            throw new WebApplicationException("Ride not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("check/")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Integer> removedRides(){
        return rideDao.checkForRemovedRides(user.getId());
    }

    @Path("completed/")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getUsersCompletedRides() {
        try {
            return rideDao.getUsersCompletedRides(user.getId());
        } catch (Exception e) {
            throw new WebApplicationException("Rides not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("upcoming/")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getUsersUpcomingRides() {
        List<Ride> rides = rideDao.getRidesWhereDriver(user.getId());
        rides.addAll(rideDao.getRidesWherePassenger(user.getId()));
        rides.removeIf(r -> r.isCompleted());
        return rides;
    }

    @Path("upcoming/{userId}")
    @GET
    @RolesAllowed(Role.Names.ADMIN)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getUsersUpcomingRidesAsAdmin(@PathParam("userId") int userId) {
        List<Ride> rides = rideDao.getRidesWhereDriver(userId);
        rides.addAll(rideDao.getRidesWherePassenger(userId));
        rides.removeIf(r -> r.isCompleted());
        return rides;
    }


    @Path("matching/{startCity}/{endCity}/{latestArrivalTime}")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getMatchingRides(@PathParam("startCity") String startCity,
                                       @PathParam("endCity") String endCity ,
                                       @PathParam("latestArrivalTime") long latestArrivalTime) {
        try {
            return rideDao.getMatchingRides(startCity, endCity, latestArrivalTime);
        } catch (Exception e) {
            throw new WebApplicationException("No matching rides", Response.Status.NOT_FOUND);
        }
    }

    @Path("all")
    @GET
    @RolesAllowed(Role.Names.ADMIN)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Ride> getAllRides() {
        try {
            return rideDao.getAllRides();
        } catch (Exception e) {
            throw new WebApplicationException("Rides not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("{rideId}")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Ride getRideFromRideId(@PathParam("rideId") int rideId) {
        try {
            return rideDao.getRideFromRideId(rideId);
        } catch (Exception e) {
            throw new WebApplicationException("Ride not found", Response.Status.NOT_FOUND);
        }
    }
}
