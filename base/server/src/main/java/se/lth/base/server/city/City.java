package se.lth.base.server.city;

public class City {
	
	private final String locationName;
	private final float latitude;
	private final float longitude;
	private final int stopDelay;
	
	public City(String locationName, float latitude, float longitude, int stopDelay) {
		this.locationName = locationName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.stopDelay = stopDelay;
	}

	public String getLocationName() {
		return locationName;
	}

	public float getLatitude() {
		return latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public int getStopDelay() {
		return stopDelay;
	}
	
	
	
}
