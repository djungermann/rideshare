package se.lth.base.server.ride;
import se.lth.base.server.Config;
import se.lth.base.server.car.CarDataAccess;
import se.lth.base.server.passenger.PassengerDataAccess;

import java.util.Comparator;

class SortRides implements Comparator<Ride>
{
    private final long preferredDropOfTime;
    private final static long TEN_MINUTES = 10 * 60 * 1000;
    private final static long HALF_HOUR = 30 * 60 * 1000;

    private PassengerDataAccess passdao = new PassengerDataAccess(Config.instance().getDatabaseDriver());
    private CarDataAccess cardao = new CarDataAccess(Config.instance().getDatabaseDriver());

    SortRides(long preferredDropOfTime) {
        this.preferredDropOfTime = preferredDropOfTime;
    }

    public int compare(Ride a, Ride b) {
        long aTimeDiff = a.getLatestArrivalTime() - (preferredDropOfTime - TEN_MINUTES);
        long bTimeDiff = b.getLatestArrivalTime() - (preferredDropOfTime - TEN_MINUTES);

        if(Math.abs((aTimeDiff-bTimeDiff)) <  HALF_HOUR){
            int aEmptySeats = cardao.getRidesCar(a.getId()).getCapacity()-passdao.getNoPassengers(a.getId());
            int bEmptySeats = cardao.getRidesCar(b.getId()).getCapacity()-passdao.getNoPassengers(b.getId());
            return Integer.compare(aEmptySeats,bEmptySeats);
        } else{
            return Long.compare(aTimeDiff, bTimeDiff);
        }

    }
}
