package se.lth.base.server.car;

import se.lth.base.server.database.DataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.Mapper;

import javax.ws.rs.WebApplicationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class CarDataAccess extends DataAccess<Car> {

    private static final class CarMapper implements Mapper<Car> {

        @Override
        public Car map(ResultSet resultSet) throws SQLException {
            return new Car(resultSet.getInt("car_id"),
                    resultSet.getInt("owner_id"),
                    resultSet.getString("reg_nbr"),
                    resultSet.getInt("capacity"),
                    resultSet.getString("model"),
                    resultSet.getString("color"));
        }
    }

    public CarDataAccess(String carUrl) {
        super(carUrl, new CarMapper());
    }


    /**
     * Add new car connected to a user.
     *
     * @param ownerId  user to add payload to.
     * @param regNbr new payload to append.
     * @param capacity new payload to append.
     * @param model new payload to append.
     * @param color new payload to append.
     * @return the created car object, containing additionally the id of the car.
     * @throws DataAccessException if the car couldn't be added
     */
    public Car addCar(int ownerId, String regNbr, int capacity, String model, String color) {
        int carId = insert("INSERT INTO car (owner_id, reg_nbr, capacity, model, color) VALUES (?,?,?,?,?)",
                ownerId, regNbr, capacity, model, color);
        return new Car(carId, ownerId, regNbr, capacity, model, color);
    }

    /**
     * Get a car with given car id.
     *
     * @param carId id of the car.
     * @return car object with given id.
     * @throws DataAccessException if the car is not found.
     */
    public Car getCar(int carId) {
        return queryFirst("SELECT * FROM car WHERE car_id = ?", carId);
    }

    /**
     * Remove a car connected to a user
     *
     * @param carId id of the car that is about to be deleted
     * @param ownerId id of the car owner
     * @return true if the car was successfully deleted
     * @throws DataAccessException if the car could not be deleted
     */
    public boolean deleteCar(int carId, int ownerId) {
        // TODO: check if car is part of any locked rides (?)
        return execute("DELETE FROM car WHERE car_id = ? AND owner_id = ?", carId, ownerId) > 0;
    }

    /**
     * Get the cars owned by a user
     *
     * @param ownerId the id of the owner
     * @return All the cars connected to an owner
     * @throws DataAccessException if the car is not found.
     */
    public List<Car> getUsersCars(int ownerId) {
        return query("SELECT * FROM car WHERE owner_id = ?", ownerId);
    }

    /**
     * Get the ride's car.
     *
     * @param rideId the id of the ride.
     * @return the car connected to the ride.
     * @throws DataAccessException if the car is not found.
     */
    public Car getRidesCar(int rideId) {
        return queryFirst("SELECT * FROM car WHERE car_id = (SELECT car_id FROM ride WHERE ride_id = ?)", rideId);
    }

}
