package se.lth.base.server.passenger;

import se.lth.base.server.Config;
import se.lth.base.server.city.City;
import se.lth.base.server.city.CityDataAccess;
import se.lth.base.server.database.DataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.Mapper;
import se.lth.base.server.ride.Ride;
import se.lth.base.server.ride.RideDataAccess;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PassengerDataAccess extends DataAccess<Passenger> {
    private final RideDataAccess rideDao = new RideDataAccess(Config.instance().getDatabaseDriver());
    private final CityDataAccess cityDao = new CityDataAccess(Config.instance().getDatabaseDriver());



    private static final class PassengerMapper implements Mapper<Passenger> {

        @Override
        public Passenger map(ResultSet resultSet) throws SQLException {
            return new Passenger(resultSet.getInt("user_id"),
                    resultSet.getInt("ride_id"),
                    resultSet.getString("pick_up_address"),
                    resultSet.getString("drop_off_address"),
                    resultSet.getObject("joined", Date.class).getTime());
        }
    }

    public PassengerDataAccess(String driverUrl) {
        super(driverUrl, new PassengerMapper());
    }

    /**
     *  Get a passenger with a given rideId and userId
     *
     * @param rideId the id of the ride
     * @param userId the id of the user
     * @return the passenger object
     * @throws DataAccessException if passenger not found
     */
    public Passenger getPassenger(int rideId, int userId) {
        return queryFirst("SELECT * FROM passenger WHERE ride_id = ? AND user_id = ?", rideId, userId);
    }

    /**
     * Get a passenger with a given rideId
     *
     * @param rideId the id of the ride
     * @return a list of passenger objects
     * @throws DataAccessException if no passengers are found
     */
    public List<Passenger> getPassengers(int rideId) {
        try{
            rideDao.getRideFromRideId(rideId);
        } catch (Exception e){
            throw e;
        }
        return query("SELECT * FROM passenger WHERE ride_id = ?", rideId);
    }

    /**
     * Deletes a passenger that is registered to a ride
     *
     * @param rideId the id of the ride that the passenger is registered to
     * @param userId the id of the user
     * @return true if the passenger was successfully deleted. Otherwise false.
     * @throws DataAccessException if the passenger can't be deleted.
     */
    public boolean deletePassenger(int rideId, int userId){
        // TODO: check if ride is locked or not
        return execute("DELETE FROM passenger WHERE ride_id = ? AND user_id = ?", rideId, userId) > 0;
    }

    /**
     * Gets the number of passengers in a ride
     *
     * @param rideId id of the ride
     * @return an integer
     * @throws DataAccessException if the number of passengers can't be retrieved.
     */
    public int getNoPassengers(int rideId){
        return query("SELECT * FROM passenger WHERE ride_id = ?",rideId).size();
    }

    /**
     * Add new passenger to a ride.
     * @param rideId ride to add to
     * @param userId user who is going to ride
     * @param pickupAddress where to pick up the passenger
     * @param dropoffAddress where to leave the passenger.
     * @return the created passenger object, containing additionally the time it of joining.
     * @throws DataAccessException if the passenger can't be added
     */
    public boolean addPassenger(int rideId, int userId, String pickupAddress, String dropoffAddress) {
        Ride r = rideDao.getRideFromRideId(rideId);
        if(rideDao.isCompleted(rideId,r.getLatestArrivalTime()) || rideDao.isLocked(rideId,r.getLatestArrivalTime(),r.getStartCity(),r.getEndCity()) || rideDao.isFull(rideId)) {
        	return false;
        	
        } else {
        		
        	insert("INSERT INTO passenger (user_id, ride_id, pick_up_address, drop_off_address) VALUES (?,?,?,?)",userId,rideId,pickupAddress,dropoffAddress);
        	return true;
        }
    }

    /**
     * Delete a passenger from a ride
     *
     * @param rideId ride to remove from
     * @param userId user who was going to ride.
     * @return true if the passenger was deleted, false otherwise.
     * @throws DataAccessException if the passenger can't be removed
     */
    public boolean removePassenger(int rideId, int userId){
    	
    	Ride r = rideDao.getRideFromRideId(rideId);

        if (rideDao.isCompleted(rideId,r.getLatestArrivalTime()) || rideDao.isLocked(rideId,r.getLatestArrivalTime(),r.getStartCity(),r.getEndCity())) {
            return false;
        }
        Passenger p = queryFirst("SELECT * FROM passenger WHERE user_id = ? AND ride_id = ?", userId, rideId);
        insert("INSERT INTO removed_from_rides(user_id, ride_id) VALUES (?, ?);", p.getUserId(), p.getRideId());
        return execute("DELETE FROM passenger WHERE user_id = ? AND ride_id = ?", userId, rideId) > 0;
    }

    /**
     * Calculates the drop-off time for a passenger.
     *
     * @param rideId the id of the ride that the passenger is registered to
     * @param userId the id of the passenger
     * @return a time representing the passengers drop-off time
     */
    public long getPassengerDropOff(int rideId, int userId){
        Passenger p = getPassenger(rideId,userId);
        Ride r = rideDao.getRideFromRideId(rideId);
        long latestArrivalTime = r.getLatestArrivalTime();
        long stopDelay = cityDao.getCity(r.getEndCity()).getStopDelay();
        List<Passenger> sortedPassengers = getPassengers(rideId).stream()
                .sorted(Comparator.comparing(Passenger::getJoined).reversed())
                .collect(Collectors.toList());
        return latestArrivalTime - stopDelay * sortedPassengers.indexOf(p);
    }

    /**
     * Calculates the pickup time for a passenger
     *
     * @param rideId the of the ride
     * @param userId the id of the passenger
     * @return a time representing the passengers pickup time
     */
    public long getPassengerPickup(int rideId, int userId){
        Passenger p = getPassenger(rideId,userId);
        Ride r = rideDao.getRideFromRideId(rideId);
        long latestArrivalTime = r.getLatestArrivalTime();
        City startCity = cityDao.getCity(r.getStartCity());
        City endCity = cityDao.getCity(r.getEndCity());
        long startDelay = startCity.getStopDelay();
        long endDelay = endCity.getStopDelay();
        List<Passenger> sortedPassengers = getPassengers(rideId).stream()
                .sorted(Comparator.comparing(Passenger::getJoined))
                .collect(Collectors.toList());

        long driveTime = rideDao.timeBetween(startCity, endCity);
        long totalDropOffTime = endDelay * sortedPassengers.size();
        return latestArrivalTime  - totalDropOffTime - driveTime - startDelay * sortedPassengers.indexOf(p);
    }
}
