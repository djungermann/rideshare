package se.lth.base.server.car;

public class Car {

    private final int carId;
    private final int ownerId;
    private final String regNbr;
    private final int capacity;
    private final String model;
    private final String color;

    public Car(int carId, int ownerId, String regNbr, int capacity, String model, String color) {
        this.carId = carId;
        this.ownerId = ownerId;
        this.regNbr = regNbr;
        this.capacity = capacity;
        this.model = model;
        this.color = color;
    }

    public int getCarId() { return carId; }

    public int getOwnerId() { return ownerId; }

    public String getRegNbr() { return regNbr; }

    public int getCapacity() { return capacity; }

    public String getModel() { return model; }

    public String getColor() { return color; }

}
