package se.lth.base.server.ride;

public class Ride{
    private final int id;
    private final int driverId;
    private final String startCity;
    private final String endCity;
    private final long latestArrivalTime;
    private final int carId;
    private final int availableCapacity;
    private final boolean full;
    private final boolean completed;
    private final boolean locked;

    public Ride(int id,
                int driverId,
                String startCity,
                String endCity,
                long latestArrivalTime,
                int carId,
                int availableCapacity,
                boolean full,
                boolean locked,
                boolean completed) {
        this.id = id;
        this.driverId = driverId;
        this.startCity = startCity;
        this.endCity = endCity;
        this.latestArrivalTime = latestArrivalTime;
        this.carId = carId;
        this.availableCapacity = availableCapacity;
        this.full = full;
        this.locked = locked;
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public int getDriverId() {
        return driverId;
    }

    public String getStartCity() { return startCity; }

    public String getEndCity() {
        return endCity;
    }

    public long getLatestArrivalTime() {
        return latestArrivalTime;
    }

    public int getCarId() {
        return carId;
    }

    public int getAvailableCapacity() {
        return availableCapacity;
    }

    public boolean isFull() { return full; }

    public boolean isLocked() { return locked; }

    public boolean isCompleted() { return completed; }

}
