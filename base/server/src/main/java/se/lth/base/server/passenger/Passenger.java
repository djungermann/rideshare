package se.lth.base.server.passenger;

public class Passenger {
    private final int userId;
    private final int rideId;
    private final String pickUpAddress;
    private final String dropOffAddress;
    private final long joined;

    public Passenger(int userId, int rideId, String pickUpAddress, String dropOffAddress, long joined) {
        this.userId = userId;
        this.rideId = rideId;
        this.pickUpAddress = pickUpAddress;
        this.dropOffAddress = dropOffAddress;
        this.joined = joined;
    }

    public int getUserId() { return userId; }

    public int getRideId() {
        return rideId;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public String getDropOffAddress() {
        return dropOffAddress;
    }

    public long getJoined() {
        return joined;
    }

}
