var base = base || {};
base.changeLocation = function(url) {
    window.location.replace(url);
};
base.signupController = (function () {
    'use strict'
    const controller = {
        load: function() {
            document.getElementById('signup-form').onsubmit = function(event) {
                event.preventDefault();
                controller.signupUser();
                return false;
            };
            base.rest.getUser().then(function(user) {
                if (!user.isNone()) {
                    base.changeLocation('/');
                }
            });
        },
        signupUser: function () {
            const accountname = document.getElementById('accountname').value;
            const displayname = document.getElementById('displayname').value;
            const phonenumber = document.getElementById('phonenumber').value;
            const password = document.getElementById('password').value;
            
            base.rest.signupUser({
                username: accountname,
                displayName: displayname,
                phoneNbr: phonenumber,
                password
            })
                .then(() => {
                    alert("Account created successfully");
                    base.changeLocation('/login/login.html');
                })
                .catch(() => {
                    document.getElementById('password').value = ''
                });
        },
        initOnLoad: function() {
            document.addEventListener('DOMContentLoaded', base.signupController.load);
        }
    };
    return controller;
})();
