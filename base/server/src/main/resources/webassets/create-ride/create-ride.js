var base = base || {};

base.createRideController = function() {
    'use strict' // add this to avoid some potential bugs

    let carModel;
    let cityModel;

    const CarViewModel = function (_car) {
        this.car = _car;

        const viewModel = this;

        this.renderCar = function(template) {
            const option = template.content.querySelector('option');
            option.textContent = `${viewModel.car.regNbr} | ${viewModel.car.color} ${viewModel.car.model}`;
            option.value = viewModel.car.carId;

            const clone = document.importNode(template.content, true);
            template.parentElement.appendChild(clone);
        }
    };

    const CityViewModel = function (_city) {
        this.city = _city;

        const viewModel = this;

        this.render = function (template) {
            this.update(template.content.querySelector('option'));
            const clone = document.importNode(template.content, true);
            template.parentElement.appendChild(clone);
        };

        this.update = function(option) {
            option.textContent = viewModel.city["locationName"];
        };
    };

    const view = {
        render: function() {
            if (carModel) {
                this.removeChildren(this.templateCar());
                carModel.forEach(d => {
                    d.renderCar(this.templateCar());
                });
            }

            if (cityModel) {
                const tStart = this.templateStart();
                cityModel.forEach(d => d.render(tStart));
                const tEnd = this.templateEnd();
                cityModel.forEach (d => d.render(tEnd));
            }
        },

        renderCapacity: function(capacity) {
            const template = this.templatePassengers();
            this.removeChildren(template);
            for (let i = 1; i <= capacity; i++) {
                const option = template.content.querySelector('option');
                option.textContent = i + " passengers";
                option.value = i;

                const clone = document.importNode(template.content, true);
                template.parentElement.appendChild(clone);
            }
        },

        templateCar: function () {
            return document.getElementById('car-template');
        },

        templatePassengers: function () {
            return document.getElementById('passengers-template');
        },

        templateStart: function() {
            return document.getElementById('start-city-template');
        },

        templateEnd: function() {
            return document.getElementById('end-city-template');
        },

        removeChildren: function (template) {
            const children = [...template.parentElement.children];
            for (const child of children) {
                if (child.tagName !== 'TEMPLATE') {
                    child.remove();
                }
            }
        },

    };

    const controller = {
        load: function () {
            document.getElementById('car').onchange = e => {
                const id = e.target.value;
                const capacity = carModel.find(x => x.car.carId == id).car.capacity;
                this.updateCapacity(capacity);
            };

            document.getElementById('create-ride-form').onsubmit = function (event) {
                event.preventDefault();
                controller.createRide();
                return false;
            };

            this.loadCars();
            this.loadCities();
        },

        updateCapacity: function(capacity) {
            view.renderCapacity(capacity);
        },

        loadCars: function () {
            base.rest.getCars().then(cars => {
                if (cars.length < 1) {
                    alert("You need to add a car before you can create a ride.");
                    return;
                }
                this.updateCapacity(cars[0].capacity);
                carModel = cars.map(c => new CarViewModel(c));
                view.render();
            });
        },

        loadCities: function () {
            base.rest.getAllCities().then(cities => {
                cityModel = cities.map(c => new CityViewModel(c));
                cityModel.push(new CityViewModel({}));
                view.render();
            });
        },

        createRide: function() {
            const carId = document.getElementById('car').value;
            const capacity = document.getElementById('passengers').value;
            
            const startCity = document.getElementById('startCity').value;
            const startAddress = document.getElementById('startAddress').value;

            const endCity = document.getElementById('dropOffCity').value;
            const endAddress = document.getElementById('dropOffAddress').value;

            const latestArrivalTime = document.getElementById('latestArrivalTime').value;
            const timestamp = new Date(latestArrivalTime).getTime();

            if (!capacity) {
                alert("You need to selct a car.");
                return;
            }

            if (startCity == endCity) {
                alert("Start and destination city can't be the same.");
                return;
            }

            if (!latestArrivalTime) {
                alert("Please select a date");
                return;
            }

            if (timestamp < Date.now()) {
                alert("Please select a later date");
                return;
            }

            base.rest.createRide({
                carId,
                availableCapacity: capacity,
                startCity,
                endCity,
                latestArrivalTime: timestamp
            }).then(() => {
                alert("Ride created successfully.");
            })
        }
    };
        return controller;
};
