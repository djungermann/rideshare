package se.lth.base.server.passenger;

import se.lth.base.server.Config;
import se.lth.base.server.user.Role;
import se.lth.base.server.user.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("passenger")
public class PassengerResource {

    private final PassengerDataAccess passengerDao = new PassengerDataAccess(Config.instance().getDatabaseDriver());

    // Current user, it is set by AuthenticationFilter. Based on a database lookup of the users login token.
    private final User user;

    public PassengerResource(@Context ContainerRequestContext context) {
        this.user = (User) context.getProperty(User.class.getSimpleName());
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    @Path("{rideId}")
    public List<Passenger> getPassengers(@PathParam("rideId") int rideId) {
        try {
            return passengerDao.getPassengers(rideId);
        } catch (Exception e) {
            throw new WebApplicationException("No ride found", Response.Status.NOT_FOUND);
        }
    }

    @POST
    @RolesAllowed(Role.Names.USER)
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public boolean addPassenger(Passenger p) {
    	System.out.println("addpassenger current user : " + user.getId() + " " + user.getDisplayName());
        boolean success = passengerDao.addPassenger(p.getRideId(),user.getId(),p.getPickUpAddress(),p.getDropOffAddress());
        if(!success) throw new WebApplicationException("Ride could not be joined, please try again", Response.Status.BAD_REQUEST);
        return true;
    }

	@Path("removePassenger/{rideId}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public boolean removePassenger(@PathParam("rideId") int rideId) {

        boolean success = passengerDao.removePassenger(rideId, user.getId());
        if(!success) throw new WebApplicationException("Passenger could not be removed", Response.Status.BAD_REQUEST);
        return true;
    }


    @GET
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    @Path("dropoff/{rideId}")
    public long getPassengerDropOff(@PathParam("rideId") int rideId) {
        try {
            return passengerDao.getPassengerDropOff(rideId, user.getId());
        } catch (Exception e) {
            throw new WebApplicationException("Drop-off time could not be calculated", Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    @Path("pickup/{rideId}")
    public long getPassengerPickUp(@PathParam("rideId") int rideId) {
        try {
            return passengerDao.getPassengerPickup(rideId, user.getId());
        } catch (Exception e) {
            throw new WebApplicationException("Pickup time could not be calculated", Response.Status.BAD_REQUEST);
        }
    }

    /*
    @Path("past/{userId}")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Passenger> getUsersPastDriverRides(@PathParam("userId") int userId){
        return passengerDao.getPassengerPastRides(userId);
    }
    */

}
