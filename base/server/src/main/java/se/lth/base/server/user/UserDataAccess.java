package se.lth.base.server.user;

import se.lth.base.server.Config;
import se.lth.base.server.database.DataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.ErrorType;
import se.lth.base.server.database.Mapper;
import se.lth.base.server.passenger.Passenger;
import se.lth.base.server.passenger.PassengerDataAccess;
import se.lth.base.server.ride.Ride;
import se.lth.base.server.ride.RideDataAccess;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Basic functionality to support standard user operations. Some notable omissions are removing user, time out on
 * sessions, getting a user by name or id, etc.
 * <p>
 * This is intended to be as realistic as possible with reasonable security (single factor authentication).
 * The security measures are as follows.
 * <ul>
 * <li>All passwords are stored in a hashed format in the database, using @{@link Credentials#generatePasswordHash(long)}}</li>
 * <li>Usernames are used to salt passwords,
 * <a href="https://en.wikipedia.org/wiki/Salt_(cryptography)">see here for explanation.</a>
 * <li>When a user does login, it receives a UUID-token. This token is then used to authenticate,
 * using @{@link #getSession}.
 * </li>
 * </ul>
 *
 * @author Rasmus Ros, rasmus.ros@cs.lth.se
 * @see DataAccess
 */
public class UserDataAccess extends DataAccess<User> {
    public final RideDataAccess rideDao = new RideDataAccess(Config.instance().getDatabaseDriver());
    private final PassengerDataAccess passDao = new PassengerDataAccess(Config.instance().getDatabaseDriver());

    private static class UserMapper implements Mapper<User> {


        @Override
        public User map(ResultSet resultSet) throws SQLException {

            return new User(resultSet.getInt("user_id"),
                            Role.valueOf(resultSet.getString("role")),
                            resultSet.getString("username"),
                            resultSet.getString("display_name"),
                            resultSet.getString("phone_nbr"),
                            resultSet.getBoolean("removed"));
        }
    }

    public UserDataAccess(String driverUrl) {
        super(driverUrl, new UserMapper());
    }

    /**
     * Add a new user to the system, this can only be done by admin as he or she
     * has the right to pick the role for the new user.
     *
     * @param credentials of the new user, containing name, role, and password.
     * @throws DataAccessException if duplicated username or too short user names.
     */
    public User addUser(Credentials credentials) {
        long salt = Credentials.generateSalt();
        int userId = insert("INSERT INTO user (role_id, username, password_hash, salt, display_name, phone_nbr) VALUES ((" +
                        "SELECT role_id FROM user_role WHERE user_role.role=?),?,?,?,?,?)",
                credentials.getRole().name(), credentials.getUsername(), credentials.generatePasswordHash(salt), salt, credentials.getDisplayName(), credentials.getPhoneNbr());
        return new User(userId, credentials.getRole(), credentials.getUsername(), credentials.getDisplayName(), credentials.getPhoneNbr(), credentials.isRemoved());
    }

    /**
     * Add a new user to the system. ROLE is default set to USER when this method is called.
     *
     * @param credentials of the new user, containing name, and password.
     * @throws DataAccessException if duplicated username or too short user names.
     */
    public User newUser(Credentials credentials) {
        long salt = Credentials.generateSalt();
        int userId = insert("INSERT INTO user (role_id, username, password_hash, salt, display_name, phone_nbr) VALUES ((" +
                                 "SELECT role_id FROM user_role WHERE user_role.role=?),?,?,?,?,?)",
                        "USER", credentials.getUsername(), credentials.generatePasswordHash(salt), salt, credentials.getDisplayName(), credentials.getPhoneNbr());
        return new User(userId, credentials.getRole(), credentials.getUsername(), credentials.getDisplayName(), credentials.getPhoneNbr(), credentials.isRemoved());
    }

    /**
     * Update user parameters. Parameters that can be updated is:
     * - password
     * - display_name
     * - phone_nbr
     * - role
     * - removed (can only be updated to FALSE, if you wish to remove a user, use removeUser().
     *
     * @param userId the id of the user to be updated
     * @param credentials the new user parameters which is to be updated to.
     * @return the User with its new parameters
     * @throws DataAccessException if the user can't be found
     */
    public User updateUser(int userId, Credentials credentials) {

        if (credentials.hasPassword()) {
            long salt = Credentials.generateSalt();
            execute("UPDATE user SET password_hash = ?, salt = ?, display_name = ?, phone_nbr = ?," +
                            " role_id = (" +
                            " SELECT user_role.role_id FROM user_role WHERE user_role.role = ?) " +
                            "WHERE user_id = ?",
                    credentials.generatePasswordHash(salt), salt,
                    credentials.getDisplayName(), credentials.getPhoneNbr(), credentials.getRole().name(), userId);
        } else {
            execute("UPDATE user SET username = ?, role_id = (" +
                            "    SELECT user_role.role_id FROM user_role WHERE user_role.role = ?) " +
                            "WHERE user_id = ?",
                    credentials.getUsername(), credentials.getRole().name(), userId);
        }
        if(!credentials.isRemoved()){
            execute("UPDATE user SET removed = FALSE WHERE user_id = ?",userId);
        }
        return getUser(userId);
    }

    /**
     * Update password, display name and/or phone number for a user.
     *
     * @param  userId id of the user that is to be updated.
     * @param credentials of the user, containing new password, display name and phone number.
     * @throws DataAccessException if duplicated username or too short user names.
     */
    public User updateUserAsUser(int userId, Credentials credentials) {
        if (credentials.hasPassword()) {
            long salt = Credentials.generateSalt();
            execute("UPDATE user SET password_hash = ?, salt = ?, display_name = ?, phone_nbr = ? " +
                            "WHERE user_id = ?",
                    credentials.generatePasswordHash(salt), salt, credentials.getDisplayName(),
                    credentials.getPhoneNbr(), userId);
        } else {
            execute("UPDATE user SET display_name = ?, phone_nbr = ? " +
                            "WHERE user_id = ?",
                    credentials.getDisplayName(), credentials.getPhoneNbr(), userId);
        }
        return getUser(userId);
    }

    /**
     * Get a user object from the userId
     * @param userId the id of the user
     * @return the User
     * @throws DataAccessException if the user can't be found
     */
    public User getUser(int userId) {
        return queryFirst("SELECT user.*, role, username FROM user, user_role " +
                "WHERE user.user_id = ? AND user.role_id = user_role.role_id", userId);
    }

    /**
     * Get a list of rides that user is part of.
     *
     * @param userId the id of the user
     * @return list of rides that user is part of.
     */
    public List<Ride> getUsersRides(int userId) {
        List<Ride> rides = rideDao.getRidesWhereDriver(userId);
        rides.addAll(rideDao.getRidesWherePassenger(userId));
        return rides;
    }

    /**
     * Deletes a user (setting it as inactive) if the user can be found and isn't part of any locked ride.
     * If user can be deleted, all upcoming rides where the user is the driver or passenger will be removed.
     *
     * @return true if user was deleted else false.
     * @throws DataAccessException if the user couldn't be removed due to the user being part of a locked ride,
     * or non existing user.
     */
    public boolean deleteUser(int userId, List<Ride> rides) {

        for(Ride r : rides) {
            if(rideDao.isLocked(r.getId(), r.getLatestArrivalTime(), r.getStartCity(), r.getEndCity())) {
                throw new WebApplicationException("User is part of locked ride", Response.Status.BAD_REQUEST);
            }
        }

        // Set user to inactive
        if (execute("UPDATE user SET removed = TRUE WHERE user_id = ? AND removed = FALSE", userId) == 0) {
            return false;
        } else {
            for (Ride r : rides) {
                if (!rideDao.isCompleted(r.getId(), r.getLatestArrivalTime())) {
                    if (userId == r.getDriverId()) {
                        rideDao.deleteRideAsDriver(r.getId(), userId);
                    } else {
                        passDao.deletePassenger(r.getId(), userId);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Reactivates a user if the user exists.
     *
     * @param userId id of the user.
     * @return true if the user was successfully reactivated, else false.
     */
    public boolean reactivateUser(int userId) {
        try {
            return execute("UPDATE user SET removed = FALSE WHERE user_id = ?", userId) > 0;
        } catch(DataAccessException e) {
            return false;
        }
    }

    /**
     * Get a list of all registered users in the system, both current and removed.
     *
     * @return all users in the system.
     * @throws DataAccessException if the user can't be found
     */
    public List<User> getUsers() {
        return query("SELECT user.*, role FROM user, user_role " +
                "WHERE user.role_id = user_role.role_id");
    }

    /**
     * Fetch session and the corresponding user.
     *
     * @param sessionId globally unique identifier, stored in the client.
     * @return session object wrapping the user.
     * @throws DataAccessException if the session is not found.
     */
    public Session getSession(UUID sessionId) {
        User user = queryFirst("SELECT user.user_id, username, role, display_name, phone_nbr, removed FROM user, user_role, session " +
                "WHERE user_role.role_id = user.role_id " +
                "    AND session.user_id = user.user_id " +
                "    AND session.session_uuid = ?", sessionId);
        execute("UPDATE session SET last_seen = CURRENT_TIMESTAMP() " +
                "WHERE session_uuid = ?", sessionId);
        return new Session(sessionId, user);
    }

    /**
     * Logout a user. This method is idempotent, meaning it is safe to repeat indefinitely.
     *
     * @param sessionId session to remove
     * @return true if the session was found, false otherwise.
     */
    public boolean removeSession(UUID sessionId) {
        return execute("DELETE FROM session WHERE session_uuid = ?", sessionId) > 0;
    }

    /**
     * Login a user.
     *
     * @param credentials username and plain text password.
     * @return New user session, consisting of a @{@link UUID} and @{@link User}.
     * @throws DataAccessException if the username or password does not match.
     */
    public Session authenticate(Credentials credentials) {
        long salt = new DataAccess<>(getDriverUrl(), (rs) -> rs.getLong(1))
                .queryStream("SELECT salt FROM user WHERE username = ?", credentials.getUsername()).findFirst()
                .orElseThrow(() -> new DataAccessException("Username or password incorrect", ErrorType.DATA_QUALITY));
        UUID hash = credentials.generatePasswordHash(salt);

        User user = queryStream("SELECT user_id, username, display_name, phone_nbr, removed, role FROM user, user_role " +
                "WHERE user_role.role_id = user.role_id " +
                "    AND username = ? " +
                "    AND password_hash = ?", credentials.getUsername(), hash).findFirst().orElseThrow(() -> new DataAccessException("Username or password incorrect", ErrorType.DATA_QUALITY));
        UUID sessionId = insert("INSERT INTO session (user_id) " +
                "SELECT user_id from USER WHERE username = ?", user.getName());
        return new Session(sessionId, user);
    }

}
