package se.lth.base.server.user;

import se.lth.base.server.Config;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.ErrorType;
import se.lth.base.server.ride.RideDataAccess;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Path("user")
public class UserResource {

    public static final String USER_TOKEN = "USER_TOKEN";

    private final ContainerRequestContext context;
    private final User user;
    private final Session session;
    private final UserDataAccess userDao = new UserDataAccess(Config.instance().getDatabaseDriver());
    private final RideDataAccess rideDao = new RideDataAccess(Config.instance().getDatabaseDriver());


    public UserResource(@Context ContainerRequestContext context) {
        this.context = context;
        this.user = (User) context.getProperty(User.class.getSimpleName());
        this.session = (Session) context.getProperty(Session.class.getSimpleName());
    }

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public User currentUser() {
        return user;
    }

    @Path("login")
    @POST
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response login(Credentials credentials,
                          @QueryParam("remember") @DefaultValue("false") boolean rememberMe) {
        System.out.println("Login initiated");
        Session newSession = userDao.authenticate(credentials);
        if(newSession.getUser().isRemoved()){
            throw(new DataAccessException("User is removed", ErrorType.DATA_QUALITY));
        }
        int maxAge = rememberMe ? (int) TimeUnit.DAYS.toSeconds(7) : NewCookie.DEFAULT_MAX_AGE;
        return Response.noContent().cookie(newCookie(newSession.getSessionId().toString(), maxAge, null)).build();
    }

    private NewCookie newCookie(String value, int maxAge, Date expiry) {
        return new NewCookie(USER_TOKEN,
                value,                                          // value
                "/rest",                                        // path
                context.getUriInfo().getBaseUri().getHost(),    // host
                NewCookie.DEFAULT_VERSION,                      // version
                "",                                             // comment
                maxAge,                                         // max-age
                expiry,                                         // expiry
                false,                                          // secure
                true);                                          // http-only

    }

    @Path("logout")
    @POST
    @PermitAll
    public Response logout() {
        userDao.removeSession(session.getSessionId());
        return Response.noContent().cookie(newCookie("", 0, new Date(0L))).build();
    }

    @Path("roles")
    @GET
    @RolesAllowed(Role.Names.ADMIN)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Set<Role> getRoles() {
        return Role.ALL_ROLES;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.ADMIN)
    public User createUser(Credentials credentials) {
        if (!credentials.hasPassword() || !credentials.validPassword()) {
            throw new WebApplicationException("Password too short", Response.Status.BAD_REQUEST);
        }
        return userDao.addUser(credentials);
    }

    @Path("new")
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @PermitAll
    public User newUser(Credentials credentials) {
        if (!credentials.hasPassword() || !credentials.validPassword()) {
            throw new WebApplicationException("Password too short", Response.Status.BAD_REQUEST);
        }
        try {
            return userDao.newUser(credentials);
        } catch(DataAccessException e) {
            if (e.getErrorType().getHttpCode() == ErrorType.DUPLICATE.getHttpCode()) {
                throw new WebApplicationException("User already exists, please choose another account name");
            } else {
                throw new WebApplicationException(e.getMessage(), Response.Status.BAD_REQUEST);
            }
        }
    }

    @Path("all")
    @GET
    @RolesAllowed(Role.Names.ADMIN)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<User> getUsers() {
        try {
            return userDao.getUsers();
        } catch (Exception e) {
            throw new WebApplicationException("User not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("{id}")
    @GET
    @RolesAllowed(Role.Names.USER)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public User getUser(@PathParam("id") int userId) {
        try {
            return userDao.getUser(userId);
        } catch (Exception e) {
            throw new WebApplicationException("User not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("{id}")
    @RolesAllowed(Role.Names.ADMIN)
    @PUT
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public User putUser(@PathParam("id") int userId, Credentials credentials) {
        if (credentials.hasPassword() && !credentials.validPassword()) {
            throw new WebApplicationException("Password too short", Response.Status.BAD_REQUEST);
        }
        if (userId == user.getId() && user.getRole().getLevel() > credentials.getRole().getLevel()) {
            throw new WebApplicationException("Can't demote yourself", Response.Status.BAD_REQUEST);
        }
        return userDao.updateUser(userId, credentials);
    }

    @Path("edit/")
    @RolesAllowed(Role.Names.USER)
    @PUT
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public User putUserAsUser(Credentials credentials) {
        if (credentials.hasPassword() && !credentials.validPassword()) {
            throw new WebApplicationException("Password too short", Response.Status.BAD_REQUEST);
        }
        try {
            return userDao.updateUserAsUser(user.getId(), credentials);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.BAD_REQUEST);
        }
    }

    @Path("{id}")
    @RolesAllowed(Role.Names.ADMIN)
    @DELETE
    public boolean deleteUser(@PathParam("id") int userId) {
        if (userId == currentUser().getId()) {
            throw new WebApplicationException("Don't delete yourself", Response.Status.BAD_REQUEST);
        }
        try {
            userDao.getUser(userId);
        } catch (Exception e) {
            throw new WebApplicationException("User not found", Response.Status.NOT_FOUND);
        }
        try {
            return userDao.deleteUser(userId, userDao.getUsersRides(userId));
        } catch(WebApplicationException e) {
            throw e;
        }
    }

    @Path("reactivate/{userId}")
    @RolesAllowed(Role.Names.ADMIN)
    @PUT
    public boolean reactivateUser(@PathParam("userId") int userId) {
        if(!userDao.reactivateUser(userId)) {
            throw new WebApplicationException("User not found", Response.Status.NOT_FOUND);
        }
        return true;
    }
}
