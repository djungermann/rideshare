package se.lth.base.server.car;

import se.lth.base.server.Config;
import se.lth.base.server.user.Role;
import se.lth.base.server.user.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("car")
public class CarResource {

    private final CarDataAccess carDao = new CarDataAccess(Config.instance().getDatabaseDriver());

    // Current user, it is set by AuthenticationFilter. Based on a database lookup of the users login token.
    private final User user;

    public CarResource(@Context ContainerRequestContext context) {
        this.user = (User) context.getProperty(User.class.getSimpleName());
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public Car addCar(Car car) {
        try {
            return carDao.addCar(user.getId(), car.getRegNbr(), car.getCapacity(), car.getModel(), car.getColor());
        } catch (Exception e) {
            throw new WebApplicationException("Car could not be added", Response.Status.BAD_REQUEST);
        }
    }

    @Path("{carId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public Car getCar(@PathParam("carId") int carId) {
       try {
           return carDao.getCar(carId);
       } catch(Exception e) {
            throw new WebApplicationException("Car not found", Response.Status.NOT_FOUND);
       }
    }

    @Path("ride/{rideId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public Car getRidesCar(@PathParam("rideId") int rideId) {
        try {
            return carDao.getRidesCar(rideId);
        } catch(Exception e) {
            throw new WebApplicationException("Car or ride not found", Response.Status.NOT_FOUND);
        }
    }

    @Path("{carId}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public void deleteCar(@PathParam("carId") int carId) {
        if(!carDao.deleteCar(carId, user.getId())) {
            throw new WebApplicationException("Car could not be deleted", Response.Status.BAD_REQUEST);
        }
    }

    @Path("user/{userId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.ADMIN)
    public List<Car> getUsersCars(@PathParam("userId") int userId) {
        try {
            return carDao.getUsersCars(userId);
        } catch (Exception e) {
            throw new WebApplicationException("Car not found", Response.Status.NOT_FOUND);
        }
    }
    
    @Path("user/")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public List<Car> getCars() {
        try {
            return carDao.getUsersCars(user.getId());
        } catch (Exception e) {
            throw new WebApplicationException("Cars not found", Response.Status.NOT_FOUND);
        }
    }
}
