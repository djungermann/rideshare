package se.lth.base.server.city;

import se.lth.base.server.Config;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.user.Role;
import se.lth.base.server.user.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("city")
public class CityResource {

    private final CityDataAccess cityDao = new CityDataAccess(Config.instance().getDatabaseDriver());
    private final User user;

    public CityResource(@Context ContainerRequestContext context) {
        this.user = (User) context.getProperty(User.class.getSimpleName());
    }

    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public List <City> getAllCities(){
    	try {
            return cityDao.getAllCities();
        } catch (Exception e) {
    	    throw new WebApplicationException("No cities found", Response.Status.NOT_FOUND);
        }
    }

    @Path("{locationName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @RolesAllowed(Role.Names.USER)
    public City getCity(@PathParam("locationName") String locationName) {
        try {
            return cityDao.getCity(locationName);
        } catch (Exception e) {
            throw new WebApplicationException("City not found", Response.Status.NOT_FOUND);
        }
    }
}
