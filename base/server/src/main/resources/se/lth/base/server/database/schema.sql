CREATE TABLE user_role (
                           role_id             TINYINT,
                           role                VARCHAR(10) NOT NULL UNIQUE,

                           PRIMARY KEY         (role_id)
);

CREATE TABLE user (
                      user_id             INT AUTO_INCREMENT NOT NULL,
                      role_id             TINYINT NOT NULL,
                      username            VARCHAR_IGNORECASE NOT NULL UNIQUE,
                      salt                BIGINT NOT NULL,
                      password_hash       UUID NOT NULL,
                      display_name        VARCHAR NOT NULL,
                      phone_nbr           VARCHAR NOT NULL,
                      removed             BOOLEAN NOT NULL DEFAULT FALSE,

                      PRIMARY KEY         (user_id),
                      FOREIGN KEY         (role_id) REFERENCES user_role (role_id),
                      CHECK               (LENGTH(username) >= 4)
);

CREATE TABLE session (
                         session_uuid        UUID DEFAULT RANDOM_UUID(),
                         user_id             INT NOT NULL,
                         last_seen           TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

                         PRIMARY KEY         (session_uuid),
                         FOREIGN KEY         (user_id) REFERENCES user(user_id) ON DELETE CASCADE
);

CREATE TABLE car (
                     car_id              INT AUTO_INCREMENT NOT NULL,
                     owner_id            INT NOT NULL,
                     reg_nbr             VARCHAR NOT NULL,
                     capacity            INT NOT NULL,
                     model               VARCHAR NOT NULL,
                     color               VARCHAR NOT NULL,

                     PRIMARY KEY         (car_id),
                     FOREIGN KEY         (owner_id) REFERENCES user(user_id) ON DELETE CASCADE,
                     CHECK               (capacity > 0)
);


CREATE TABLE city (
                      location_name       VARCHAR NOT NULL,
                      latitude            REAL NOT NULL,
                      longitude           REAL NOT NULL,
                      stop_delay          INT NOT NULL,

                      PRIMARY KEY         (location_name)
);

CREATE TABLE ride (
                      ride_id                 INT AUTO_INCREMENT,
                      driver_id               INT NOT NULL,
                      start_city              VARCHAR NOT NULL,
                      end_city                VARCHAR NOT NULL,
                      latest_arrival_time     TIMESTAMP NOT NULL,
                      car_id                  INT NOT NULL,
                      available_capacity      INT NOT NULL,

                      PRIMARY KEY             (ride_id),
                      FOREIGN KEY             (driver_id) REFERENCES user(user_id) ON DELETE CASCADE,
                      FOREIGN KEY             (car_id) REFERENCES car(car_id),
                      FOREIGN KEY             (start_city) REFERENCES city(location_name),
                      FOREIGN KEY             (end_city) REFERENCES city(location_name),
                      CHECK                   (available_capacity > 0)
);

CREATE TABLE passenger (
                           user_id                 INT NOT NULL,
                           ride_id                 INT NOT NULL,
                           pick_up_address         VARCHAR NOT NULL,
                           drop_off_address        VARCHAR NOT NULL,
                           joined                  TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

                           PRIMARY KEY             (user_id, ride_id),
                           FOREIGN KEY             (user_id) REFERENCES user(user_id) ON DELETE CASCADE,
                           FOREIGN KEY             (ride_id) REFERENCES ride(ride_id) ON DELETE CASCADE
);

CREATE TABLE removed_from_rides (
                                    rm_id              INT AUTO_INCREMENT,
                                    user_id            INT NOT NULL,
                                    ride_id            INT NOT NULL,

                                    PRIMARY KEY (rm_id)
);

INSERT INTO user_role
VALUES (1, 'ADMIN'), (2, 'USER');

INSERT INTO user (role_id, username, salt, password_hash, display_name, phone_nbr, removed)
VALUES (1, 'Admin', -2883142073796788660, '8dc0e2ab-4bf1-7671-c0c4-d22ffb55ee59', 'JohnD', '123', false),
       (2, 'Test', 5336889820313124494, '144141f3-c868-85e8-0243-805ca28cdabd', 'JaneD', '1234', false),
       (2, 'Test2', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'Arn', '2345', false),
       (2, 'testDriver', 5336889820313124494, '144141f3-c868-85e8-0243-805ca28cdabd', 'testDriver', '123', false),
       (2, 'testPassenger', 5336889820313124494, '144141f3-c868-85e8-0243-805ca28cdabd', 'testPassenger', '1234', false),
       (2, 'testRemoved', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'testRemoved', '2345', true),
       (2, 'testUser', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'testUser', '2345', false),
       (1, 'testAdmin', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'testAdmin', '2345', false),
       (1, 'loginAdmin', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'testAdmin', '2345', false),
       (2, 'loginUser', 14021300522317557, 'e3b345e0-c040-60f5-eb0a-a5b781f03224', 'testAdmin', '2345', false);

INSERT INTO city (location_name, latitude, longitude, stop_delay)
VALUES  ('Båstad', 56.4291666666667, 12.8786111111111, 5),
        ('Bjuv', 56.0733333333333, 12.9416666666667, 5),
        ('Bromölla', 56.0755555555556, 14.4741666666667, 5),
        ('Eslöv', 55.8386111111111, 13.3, 5),
        ('Hässleholm', 56.1594444444444, 13.7783333333333, 5),
        ('Helsingborg', 56.0425, 12.7211111111111, 7),
        ('Höganäs', 56.1961111111111, 12.5769444444444, 5),
        ('Höör', 55.9097222222222, 13.5472222222222, 5),
        ('Hörby', 55.8508333333333, 13.6636111111111, 5),
        ('Kävlinge', 55.7969444444444, 13.1138888888889, 5),
        ('Klippan', 56.1347222222222, 13.1475, 5),
        ('Kristianstad', 56.0205555555556, 14.1255555555556, 5),
        ('Landskrona', 55.8794444444444, 12.8369444444444, 5),
        ('Lomma', 55.6747222222222, 13.08, 5),
        ('Lund', 55.7083333333333, 13.1991666666667, 7),
        ('Malmö', 55.5930555555556, 13.0213888888889, 7),
        ('Osby', 56.3797222222222, 13.995, 5),
        ('Perstorp', 56.1375, 13.3905555555556, 5),
        ('Simrishamn', 55.5508333333333, 14.3477777777778, 5),
        ('Sjöbo', 55.6336111111111, 13.6969444444444, 5),
        ('Skurup', 55.4819444444444, 13.5038888888889, 5),
        ('Staffanstorp', 55.6402777777778, 13.2091666666667, 5),
        ('Svalöv', 55.9141666666667, 13.1044444444444, 5),
        ('Svedala', 55.5136111111111, 13.2322222222222, 5),
        ('Tomelilla', 55.5472222222222, 13.9472222222222, 5),
        ('Trelleborg', 55.3722222222222, 13.1811111111111, 5),
        ('Vellinge', 55.4747222222222, 13.0322222222222, 5),
        ('Ystad', 55.4347222222222, 13.8252777777778, 5),
        ('Åstorp', 56.1308333333333, 12.9338888888889, 5),
        ('Ängelholm', 56.24, 12.86, 5),
        ('Örkelljunga', 56.2836111111111, 13.2880555555556, 5);

INSERT INTO car (owner_id, reg_nbr, capacity, model, color) VALUES (1, 'ADMIN', 4, 'Volvo 740', 'Blue'), (2, 'LOL123', 4, 'Volvo 740', 'Blue'), (4, 'DRIVER', 4, 'Saab 95', 'Green');



INSERT INTO ride (driver_id, start_city, end_city, latest_arrival_time, car_id, available_capacity)
VALUES (1, 'Malmö', 'Lund', '2019-12-1 13:30:39.000', 1, 3),
       (1, 'Lund', 'Malmö', '2019-12-2 13:30:39.000', 1, 4),
       (1, 'Malmö', 'Åstorp', '2019-12-1 16:30:39.000', 2, 3),
       (4, 'Malmö', 'Åstorp', '2018-12-12 16:30:39.000', 3, 3),
	   (2, 'Malmö', 'Åstorp', DATEADD(minute, 30 ,CURRENT_TIMESTAMP), 3, 3),
       (4, 'Malmö', 'Åstorp', DATEADD(month, 1 ,CURRENT_TIMESTAMP), 3, 3);


INSERT INTO passenger (user_id, ride_id, pick_up_address, drop_off_address, joined)
VALUES (2, 1, 'Exempelvägen 1', 'Exempelvägen 2', '2019-11-29 13:30:39.000'),
       (5, 6, 'Exempelvägen 1', 'Exempelvägen 2', '2018-11-29 13:30:39.000');

       