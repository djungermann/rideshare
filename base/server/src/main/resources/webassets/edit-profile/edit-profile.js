var base = base || {};

base.editProfileController = function () {
	'use strict'

	let userModel;
	let carsModel;

	const UserViewModel = function(_user) {
		this.user = _user;

		const viewModel = this;

		this.render = function(template) {
			const clone = document.importNode(template.content, true);

			clone.querySelector("#accountName").value = viewModel.user.username;
			clone.querySelector("#displayName").value = viewModel.user.displayName;
			clone.querySelector("#phoneNumber").value = viewModel.user.phoneNbr;

			template.parentElement.appendChild(clone);
		};

	};

	const CarViewModel = function (_car) {
		this.car = _car;

		const viewModel = this;

		this.render = function (template) {
			const clone = document.importNode(template.content, true);

			const button = clone.querySelector("button");

			if (viewModel.car.regNbr) {
				clone.querySelector(".numberPlate").value = viewModel.car.regNbr;
				clone.querySelector(".color").value = viewModel.car.color;
				clone.querySelector(".model").value = viewModel.car.model;
				clone.querySelector(".capacity").value = viewModel.car.capacity;

				const inputs = clone.querySelectorAll("input");
				for (const input of inputs) {
					input.disabled = true;
				}

				button.classList.add("btn-danger");
				button.textContent = "Remove Car";

				button.onclick = function(event) {
					controller.removeCar(viewModel.car.carId);
					return false;
				};
			} else {
				button.classList.add("btn-primary");
				button.textContent = "Add Car";

				button.onclick = function(event) {
					controller.addCar();
					return false;
				};
			}

			template.parentElement.appendChild(clone);
		};

	};

	const view = {
		render: function () {
			if (userModel) {
				const profileTemplate = this.profileTemplate();
				this.removeChildren(profileTemplate);
				userModel.render(profileTemplate);
			}

			if (carsModel) {
				const carTemplate = this.carTemplate();
				this.removeChildren(carTemplate);
				carsModel.forEach(d => d.render(carTemplate));
			}
		},

		profileTemplate: function () {
			return document.getElementById('edit-profile-template');
		},

		carTemplate: function() {
			return document.getElementById('car-template');
		},

		removeChildren: function (template) {
			const children = [...template.parentElement.children];
			for (const child of children) {
				if (child.tagName !== 'TEMPLATE') {
					child.remove();
				}
			}
		}
	};

	const controller = {
		load: function () {
			document.getElementById('edit-profile-form').onsubmit = function(event) {
				event.preventDefault();
				controller.updateProfile();
				return false;
			};

			this.loadProfile();
			this.loadCars();
		},

		loadProfile: function() {
			base.rest.getUser().then(user => {
				userModel = new UserViewModel(user);
				view.render();
			});
		},

		loadCars: function() {
			base.rest.getCars().then(cars => {
				carsModel = cars.map(c => new CarViewModel(c));
				carsModel.push(new CarViewModel({}));
				view.render();
			});
		},

		updateProfile: function() {
			const password = document.querySelector("#password").value;
			const displayName = document.querySelector("#displayName").value;
			const phoneNumber = document.querySelector("#phoneNumber").value;

			const profile = {
				displayName,
				phoneNbr: phoneNumber,
				password: password || null,
			};

			console.log("profile", profile);

			base.rest.updateProfile(profile).then(() => {
				this.loadProfile();
				alert("Profile successfully updated");
			});
		},

		removeCar: function(carId) {
			base.rest.deleteCar(carId).then(this.loadCars);
		},

		addCar: function() {
			const newCarDiv = document.querySelectorAll("#car-wrapper > .form-row:last-child")[0];
			
			const numberPlate = newCarDiv.querySelector(".numberPlate").value;
			const color = newCarDiv.querySelector(".color").value;
			const model = newCarDiv.querySelector(".model").value;
			const capacity = newCarDiv.querySelector(".capacity").value;

			if (!numberPlate || !color || !model || !capacity) {
				alert("Please fill in all fields");
				return;
			}

			base.rest.addCar({
				regNbr: numberPlate,
				capacity,
				model,
				color
			}).then(this.loadCars);
		},
	};

	return controller;
};