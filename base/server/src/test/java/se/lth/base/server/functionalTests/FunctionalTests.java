package se.lth.base.server.functionalTests;

import org.junit.Assert;
import org.junit.Test;
import se.lth.base.server.BaseResourceTest;
import se.lth.base.server.Config;
import se.lth.base.server.car.Car;
import se.lth.base.server.car.CarDataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.passenger.PassengerDataAccess;
import se.lth.base.server.ride.Ride;
import se.lth.base.server.ride.RideDataAccess;
import se.lth.base.server.user.*;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.GenericType;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class FunctionalTests extends BaseResourceTest {

    private final UserDataAccess userDao = new UserDataAccess(Config.instance().getDatabaseDriver());
    private final RideDataAccess rideDao = userDao.rideDao;
    private final PassengerDataAccess passengerDao = new PassengerDataAccess(Config.instance().getDatabaseDriver());
    private final CarDataAccess carDao = new CarDataAccess(Config.instance().getDatabaseDriver());

    private static final GenericType<List<User>> USER_LIST = new GenericType<List<User>>() {
    };
    /* Functional tests implemented in accordance with SVVI v1.1
     * See section "Functional tests" for descriptions of each test.
     * Tests are named in accordance with the correspoding functional tests specified in the SVVI.*/

    @Test
    //FT1 Successful login, non administrator
    public void FT1() {
        Session session = userDao.authenticate(TEST_USER_CREDENTIALS);
        assertNotNull(session.getUser());
    }

    @Test(expected = DataAccessException.class)
    //FT2 Login, wrong password for correct username
    public void FT2() {
        Session session = userDao.authenticate(TEST_USER_WRONG_CREDENTIALS);
    }

    @Test(expected = DataAccessException.class)
    //FT3 Login, non existing username
    public void FT3() {
        Session session = userDao.authenticate(TEST_NONEXISISTENT_USER_CREDENTIALS);
    }

    @Test(expected = DataAccessException.class)
    //FT4 The user tries to log in to an account previously flagged as “removed” by an admin [SRS req. 6.2.8
    public void FT4() {
        Session session = userDao.authenticate(TEST_REMOVED_USER_CREDENTIALS);
    }

    @Test(expected = DataAccessException.class)
    //FT5 Unsuccessful attempt to create a new user, username already in use
    public void FT5() {
        userDao.addUser(new Credentials("testUserFT", "password", Role.USER, "-", "-", false));
        userDao.addUser(new Credentials("testUserFT", "password", Role.USER, "-", "-", false));
    }

    @Test
    //FT7 Successful account creation
    public void FT6() {
        User newUser = userDao.addUser(new Credentials("newTestUserFT", "password", Role.USER, "-", "-", false));
        Assert.assertTrue((newUser != null));
    }

    @Test(expected = DataAccessException.class)
    //FT7 Creation of ride, the chosen arrival time creates too short of a time window
    public void FT7() {
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");
        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", System.currentTimeMillis(), car.getCarId(), 3);
    }

    @Test
    //FT8 Ride search, the user successfully searches for a ride and gets results
    public void FT8() {
        String timeString = "2019-12-15 13:30:39";
        Timestamp time = Timestamp.valueOf(timeString);
        long latestArrivalTime = time.getTime();
        User user = userDao.addUser(new Credentials("testPassengerFT", "password", Role.USER, "-", "-", false));
        rideDao.addRide(user.getId(), "Malmö", "Lund", latestArrivalTime, 2, 3);
        List<Ride> list = rideDao.getMatchingRides("Malmö", "Lund", latestArrivalTime);
        assertFalse(list.isEmpty());
    }

    @Test
    //FT9 Join one of the rides from the search result
    public void FT9() {
        String timeString = "2019-12-15 14:30:39";
        Timestamp time = Timestamp.valueOf(timeString);
        long latestArrivalTime = time.getTime();

        User passenger = userDao.addUser(new Credentials("testPassengerFT", "password", Role.USER, "-", "-", false));
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");
        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", latestArrivalTime, car.getCarId(), 3);
        List<Ride> list = rideDao.getMatchingRides("Malmö", "Lund", latestArrivalTime);

        assertTrue(passengerDao.addPassenger(list.get(0).getId(), passenger.getId(), "Address", "Address"));
    }

    @Test
    //FT10 View rides, a passenger remove themselves from a ride
    public void FT10() {
        User user = userDao.addUser(new Credentials("testPassengerFT", "password", Role.USER, "-", "-", false));
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");
        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", System.currentTimeMillis() + 1000000000, car.getCarId(), 3);

        passengerDao.addPassenger(ride.getId(), user.getId(), "Address", "Address");
        assertTrue(passengerDao.deletePassenger(ride.getId(), user.getId()));
    }

    @Test
    //FT11 View rides, a driver removes an upcoming ride
    public void FT11() {
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");
        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", System.currentTimeMillis() + 1000000000, car.getCarId(), 3);

        assertTrue(rideDao.deleteRideAsDriver(ride.getId(), driver.getId()));
    }

    @Test
    //FT12 Edit profile, a user can change their own profile information
    public void FT12() {
        User user = userDao.addUser(new Credentials("testUserFT", "password", Role.USER, "-", "-", false));
        String oldUserName = userDao.getUser(user.getId()).getDisplayName();
        userDao.updateUserAsUser(user.getId(), new Credentials("testUserFT", "password", Role.USER, "newName", "-", false));
        String newUserName = userDao.getUser(user.getId()).getDisplayName();
        assertNotEquals(oldUserName, newUserName);
    }

    @Test
    //FT13 Edit profile, a user can add a new car to their profile
    public void FT13() {
        User user = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(user.getId(), "ABC123", 4, "Ferrari California", "Red");
        assertNotNull(car);
    }

    @Test
    //FT14 Successful logout, non administrator
    public void FT14() {
        Session session = userDao.authenticate(TEST_USER_CREDENTIALS);
        assertTrue(userDao.removeSession(session.getSessionId()));
        assertFalse(userDao.removeSession(session.getSessionId()));
    }

    @Test
    //FT15 Successful login, administrator
    public void FT15() {
        Session session = userDao.authenticate(TEST_ADMIN_CREDENTIALS);
        assertNotNull(session.getUser());
    }

    @Test
    //FT16 Administrating users, an admin flags a passenger as “removed”
    public void FT16() {
        User passenger = userDao.addUser(new Credentials("testPassengerFT", "password", Role.USER, "-", "-", false));
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");

        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", System.currentTimeMillis() + 1000000000, car.getCarId(), 3);

        passengerDao.addPassenger(ride.getId(), passenger.getId(), "Test", "Test");
        userDao.deleteUser(passenger.getId(), rideDao.getRidesWherePassenger(passenger.getId()));

        assertTrue(userDao.getUser(passenger.getId()).isRemoved());
        assertTrue(rideDao.getRidesWherePassenger(passenger.getId()).isEmpty());
    }

    @Test
    //FT17 Administrating users, an admin flags a driver as “removed”
    public void FT17() {
        User driver = userDao.addUser(new Credentials("testDriverFT", "password", Role.USER, "-", "-", false));
        Car car = carDao.addCar(driver.getId(), "TES123", 3, "Volvo", "Blue");

        Ride ride = rideDao.addRide(driver.getId(), "Malmö", "Lund", System.currentTimeMillis() + 1000000000, car.getCarId(), 3);

        userDao.deleteUser(driver.getId(), rideDao.getRidesWherePassenger(driver.getId()));
        assertTrue(userDao.getUser(driver.getId()).isRemoved());
        assertTrue(rideDao.getRidesWherePassenger(driver.getId()).isEmpty());
    }

    /* This test needs to access the database due not being able to make a locked ride from API.
     *  A ride, that is locked, exists in the database with ID = 4. */

    @Test(expected = WebApplicationException.class)
    //FT18 Administrating users, an admin tries to flag a user as “removed” from a locked ride and is unable to do so
    public void FT18() {
        Ride ride = rideDao.getRideFromRideId(5);
        userDao.deleteUser(ride.getDriverId(), rideDao.getRidesWhereDriver(ride.getDriverId()));
    }

    @Test
    //FT19 Administrating users, an admin reactivates a previously removed account
    public void FT19() {
        User user = userDao.addUser(new Credentials("testRemovedUserFT", "password", Role.USER, "-", "-", false));

        // Check if the user is successfully deleted
        assertTrue(userDao.deleteUser(user.getId(), new LinkedList<Ride>()));
        assertTrue(userDao.getUser(user.getId()).isRemoved());

        // Reactivate user and check if the user has been successfully reactivated
        assertTrue(userDao.reactivateUser(user.getId()));
        assertFalse(userDao.getUser(user.getId()).isRemoved());
    }

    /* This test needs to access the database due not being able to make a locked ride from API.
     *  A ride, that is locked, exists in the database with ID = 4. */
    @Test
    //FT20 View rides, the rides will automatically be tagged as completed after the arrival time
    public void FT20() throws InterruptedException {
        Ride ride = rideDao.getRideFromRideId(4);
        assertTrue(ride.isCompleted());
    }
}
