var base = base || {};
base.rest = (function() {
    'use strict'

    // The available classes are defined here, feel free to move them to other files when this gets unwieldy.
    // Note the pattern where the class constructor takes an object that is parsed from JSON, and extends
    // itself using the JSON object with Object.assign.
    // In this way, we don't have to write: this.id = json.id; this.payload = json.payload etc.

    const Role = function(role) {
        this.name = role;
        this.label = this.name[0] + this.name.toLowerCase().slice(1);
    };

    const User = function(json) {
        Object.assign(this, json);
        this.role = new Role(json.role);
        this.json = json;

        this.isAdmin = function() {
            return this.role.name === 'ADMIN';
        };
        this.isNone = function() {
            return this.role.name === 'NONE';
        };
    };

    const City = function(json) {
        Object.assign(this, json);
    };

    const Car = function(json) {
        Object.assign(this, json);
    };

    const Ride = function (json) {
        Object.assign(this, json);
    };

    const Passenger = function (json) {
        Object.assign(this, json);
    }

    // Expose the classes to base module, they are primarily used by the tests.
    base.User = User;
    base.Role = Role;
    base.City = City;
    base.Car = Car;
    base.Ride = Ride;
    base.Passenger = Passenger;

    // This method extends the functionality of fetch by adding default error handling.
    // Using it is entirely optional.
    const baseFetch = function(url, config) {

         // We create config if it does not already exist
        config = config || {};
         // Setting 'same-origin' make sure that cookies are sent to the server (which it would not otherwise)
        config.credentials = 'same-origin';

        return fetch(url, config)
            .then(function(response) {
                if (!response.ok) {
                    return new Promise((resolve) => resolve(response.json()))
                        .then(function(errorJson) {
                            const status = errorJson.status;
                            throw Error(`${errorJson.status} ${errorJson.error}\n${errorJson.message}`);
                        });
                } else {
                    return response;
                }
            }).catch(function(error) {
                alert(error);
                throw error;
            });
    };

    const jsonHeader = {'Content-Type': 'application/json;charset=utf-8'};

    return {
        /*
         * Fetches the currently logged in user
         *
         * example: const me = base.rest.getUser();
         */
        getUser: function() {
            return baseFetch('/rest/user')
                .then(response => response.json())
                .then(u => new User(u));
        },

        /*
         * Login with given credentials.
         * username: name of the user
         * password: password in plaintext
         * rememberMe: boolean flag, whether the user
         *
         * example: base.rest.login('test', 'password', true);
         */
        login: function(username, password, rememberMe) {
            var loginObj = {username: username, password: password};
            return baseFetch('/rest/user/login?remember=' + rememberMe, {
                    method: 'POST',
                    body: JSON.stringify(loginObj),
                    headers: jsonHeader});
        },

        /*
         * Logout the current user.
         *
         * example: base.rest.logout();
         */
        logout: function() {
            return baseFetch('/rest/user/logout', {method: 'POST'});
        },

        /*
         * Gets the users available (admin only).
         * returns: A list of User
         *
         * example: const userList = base.rest.getUsers();
         */
        getUsers: function() {
            return baseFetch('/rest/user/all')
                .then(response => response.json())
                .then(users => users.map(u => new User(u)));
        },

        /*
         * Gets the roles available (admin only).
         * returns: A list of Role
         *
         * example: const availableRoles = base.rest.getRoles();
         */
        getRoles: function() {
            return baseFetch('/rest/user/roles')
                .then(response => response.json())
                .then(roles => roles.map(r => new Role(r)));
        },

        /*
         * Add a new user (admin only).
         * credentials: object with username, password, and role
         * returns: the updated User
         *
         * example: let user = base.rest.addUser(2, {'username': 'Test2', 'password': 'password2', 'role': 'USER');
         */
        addUser: function(credentials) {
            return baseFetch('/rest/user', {
                    method: 'POST',
                    body: JSON.stringify(credentials),
                    headers: jsonHeader})
                .then(response => response.json())
                .then(u => new User(u));
        },

        /*
         * Replace a specific user with a given userId (admin only).
         * id: user to replace
         * credentials: object with username, password (optional), and role
         * returns: the updated user
         *
         * example: let user = base.rest.putUser(2, {'username': 'Test2', 'role': 'USER');
         */
        putUser: function(id, credentials) {
            return baseFetch('/rest/user/'+id, {
                    method: 'PUT',
                    body: JSON.stringify(credentials),
                    headers: jsonHeader})
                .then(response => response.json())
                .then(u => new User(u));
        },

        /*
         * Delete a specific user with a given userId (admin only).
         * id: user to delete
         *
         * example: base.rest.deleteUser(2);
         */
        deleteUser: function(userId) {
            return baseFetch('/rest/user/'+userId, {method: 'DELETE'});
        },

        reactivateUser: function(userId) {
            return baseFetch('/rest/user/reactivate/' + userId, {
                method: 'PUT',
                headers: jsonHeader
            });
        },

        // NEW ENDPOINTS

        // USER ENDPOINTS
        updateProfile: function (credentials) {
            return baseFetch('/rest/user/edit', {
                    method: 'PUT',
                    body: JSON.stringify(credentials),
                    headers: jsonHeader
                })
                .then(response => response.json())
                .then(u => new User(u));
        },
        getOtherUser: function (userId) {
            return baseFetch('/rest/user/' + userId)
                .then(response => response.json())
                .then(u => new User(u));
        },
        signupUser: function(payload) {
            return baseFetch('/rest/user/new', {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: jsonHeader
            });
        },

        // CITY ENDPOINTS
        getAllCities: function() {
            return baseFetch('/rest/city/all')
                .then(response => response.json())
                .then(cities => cities.map(c => new City(c)));
        },

        // CARS ENDPOINTS
        getCars: function() {
            return baseFetch('/rest/car/user')
                .then(response => response.json())
                .then(cars => cars.map(c => new Car(c)));
        },
        addCar: function (car) {
            return baseFetch('/rest/car', {
                    method: 'POST',
                    body: JSON.stringify(car),
                    headers: jsonHeader
                })
                .then(response => response.json())
                .then(c => new Car(c));
        },
        deleteCar: function (carId) {
            return baseFetch('/rest/car/' + carId, {
                method: 'DELETE'
            });
        },
        getCarById: function(carId) {
            return baseFetch('/rest/car/' + carId)
                .then(response => response.json())
        },
        getCarByRideId: function (rideId) {
            return baseFetch('/rest/car/ride/' + rideId)
                .then(response => response.json())
        },

        // PASSENGER ENDPOINTS
        joinRide(payload) {
            return baseFetch('/rest/passenger', {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: jsonHeader
            });
        },

        leaveRide(rideId) {
            return baseFetch('/rest/passenger/removepassenger/' + rideId, {
                method: 'DELETE'
            });
        },

        getPassengers(rideId) {
            return baseFetch(`/rest/passenger/${rideId}`)
                .then(response => response.json())
                .then(passengers => passengers.map(p => new Passenger(p)));
        },

        getPickup(rideId) {
            return baseFetch('/rest/passenger/pickup/' + rideId)
                .then(response => response.json());
        },

        getDropoff(rideId) {
            return baseFetch('/rest/passenger/dropoff/' + rideId)
                .then(response => response.json());
        },

        // RIDE ENDPOINTS
        findRide: function (payload) {
            return baseFetch(`/rest/ride/matching/${payload.startCity}/${payload.endCity}/${payload.latestArrivalTime}`)
                .then(response => response.json())
                .then(rides => rides.map(r => new Ride(r)));
        },

        getCompletedRides() {
            return baseFetch('/rest/ride/completed')
                .then(response => response.json())
                .then(rides => {
                    rides.map(r => new Ride(r));
                    console.log("completed rides:", rides);
                    return rides;
                });
        },

        getUpcomingRides() {
            return baseFetch('/rest/ride/upcoming')
                .then(response => response.json())
                .then(rides => {
                    rides.map(r => new Ride(r));
                    console.log("upcoming rides:", rides);
                    return rides;
                });
        },

        createRide(payload) {
            return baseFetch('/rest/ride', {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: jsonHeader
            });
        },

        deleteRide(rideId) {
            return baseFetch('/rest/ride/' + rideId, {
                method: 'DELETE'
            });
        },

        getDriverRides(driverId) {
            /*
            return baseFetch('/rest/ride/driver/' + driverId)
                .then(response => response.json());
            */
            return baseFetch('/rest/ride/upcoming/' + driverId)
                .then(response => response.json())
                .then(rides => {
                    rides.map(r => new Ride(r));
                    console.log("upcoming rides:", rides);
                    return rides;
                });
        }
    };
})();

/*
getAllRides: function () {
    const rides = [{
            rideId: 1,
            isDriver: true,
            arrivalTime: "12:23 27 juli 2019",
            destinationCity: "Stockholm",
            car: "ABC123 | Röd Volvo v70",
            passengers: [{
                    name: "Daniel Regefalk",
                    phone: "0707900275",
                    pickupTime: "12:23 27 juli 2019",
                    pickupAddress: "Södra Esplanaden 36",
                    dropoffTime: "14:23 27 juli 2019",
                    dropoffAddress: "Södra Esplanaden 37"
                },
                {
                    name: "Daniella Regefalk",
                    phone: "0707900275",
                    pickupTime: "12:23 27 juli 2019",
                    pickupAddress: "Södra Esplanaden 36",
                    dropoffTime: "14:23 27 juli 2019",
                    dropoffAddress: "Södra Esplanaden 37"
                }
            ]
        },

        {
            rideId: 2,
            isDriver: false,
            arrivalTime: "12:23 27 juli 2019",
            destinationCity: "Perth",
            car: "ABC123 | Röd Volvo v70",
            passengers: [{
                    name: "Daniel Regefalk",
                    phone: "0707900275",
                    pickupTime: "12:23 27 juli 2019",
                    pickupAddress: "Södra Esplanaden 36",
                    dropoffTime: "14:23 27 juli 2019",
                    dropoffAddress: "Södra Esplanaden 37"
                },
                {
                    name: "Daniella Regefalk",
                    phone: "0707900275",
                    pickupTime: "12:23 27 juli 2019",
                    pickupAddress: "Södra Esplanaden 36",
                    dropoffTime: "14:23 27 juli 2019",
                    dropoffAddress: "Södra Esplanaden 37"
                }
            ]
        }
    ];
    return Promise.resolve([
        ...rides,
        ...rides
    ])
},
*/
