package se.lth.base.server.city;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import se.lth.base.server.database.DataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.Mapper;

public class CityDataAccess extends DataAccess<City> {
	
	private static final class CityMapper implements Mapper<City> {
		 @Override
	        public City map(ResultSet resultSet) throws SQLException {
	            return new City(resultSet.getString("location_name"),
								resultSet.getFloat("latitude"),
								resultSet.getFloat("longitude"),
								resultSet.getInt("stop_delay"));
	        }
	}
	
	public CityDataAccess(String driverUrl) {
		super(driverUrl, new CityMapper());
	}

	/**
	 * Get all cities
	 *
	 * @return all city objects.
	 * @throws DataAccessException if no cities can be found
	 */
	public List<City> getAllCities(){
		return query("SELECT *  FROM city");
	}


	/**
	 * Get a city
	 *
	 * @param cityName name of the city
	 * @return given city object.
     * @throws DataAccessException if the city can't be found
	 */
	public City getCity(String cityName){
		return queryFirst("SELECT * FROM city WHERE location_name = ?", cityName);
	}
	
}
