package se.lth.base.server.ride;

import se.lth.base.server.Config;
import se.lth.base.server.city.City;
import se.lth.base.server.city.CityDataAccess;
import se.lth.base.server.database.DataAccess;
import se.lth.base.server.database.DataAccessException;
import se.lth.base.server.database.ErrorType;
import se.lth.base.server.database.Mapper;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class RideDataAccess extends DataAccess<Ride> {
    private final CityDataAccess cityDao = new CityDataAccess(Config.instance().getDatabaseDriver());

    private class RideMapper implements Mapper<Ride> {

        public Ride map(ResultSet resultSet) throws SQLException {
            int rideId = resultSet.getInt("ride_id");
            long latestArrivalTime = resultSet.getObject("latest_arrival_time",Date.class).getTime();
            String startCity = resultSet.getString("start_city");
            String endCity = resultSet.getString("end_city");
            return new Ride(rideId,
                    resultSet.getInt("driver_id"),
                    startCity,
                    endCity,
                    latestArrivalTime,
                    resultSet.getInt("car_id"),
                    resultSet.getInt("available_capacity"),
                    isFull(rideId),
                    isLocked(rideId,latestArrivalTime, startCity, endCity),
                    isCompleted(rideId,latestArrivalTime));
        }
    }

    public RideDataAccess(String driverUrl) {
        super(driverUrl);
        super.setMapper(new RideMapper());
    }

    /**
     * Add new ride connected to a user.
     *
     * @param driverId  user to add ride to.
     * @param startCity start city of the ride.
     * @param endCity destination of the ride.
     * @param carId id of car used in ride.
     * @param availableCapacity number of available seats in ride.
     * @param arrivalTime latest arrival time of the ride.
     * @return the created ride object, containing additionally the id of the ride.
     * @throws DataAccessException if the ride couldn't be added
     */
    public Ride addRide(int driverId, String startCity, String endCity, long arrivalTime, int carId, int availableCapacity) {
        if (arrivalTime - timeBetween(cityDao.getCity(startCity), cityDao.getCity(endCity)) < System.currentTimeMillis()) {
            throw new DataAccessException("Please choose a later arrival time", ErrorType.DATA_QUALITY);
        }
        int rideId = insert("INSERT INTO ride (driver_id, start_city, end_city, latest_arrival_time, car_id, available_capacity) VALUES (?,?,?,?,?,?)",
                driverId, startCity, endCity, new Timestamp(arrivalTime).toString(), carId, availableCapacity);
        return new Ride(rideId, driverId, startCity, endCity, arrivalTime, carId, availableCapacity, false, false, false);
    }

    /**
     * Delete a ride as admin by the driver userId
     *
     * @param rideId ride to remove
     * @return true if the ride was deleted, false otherwise.
     */
    public boolean deleteRideAsAdmin(int rideId) {

        Ride r = getRideFromRideId(rideId);
        if(isCompleted(rideId, r.getLatestArrivalTime()) || isFull(rideId) || isLocked(rideId, r.getLatestArrivalTime(), r.getStartCity(), r.getEndCity())) {
            return false;
        }
        DataAccess<Integer> counter = new DataAccess<>(getDriverUrl(), (rs) -> rs.getInt("user_id"));
        List<Integer> passengers = counter.query("SELECT user_id FROM passenger WHERE ride_id = ?", rideId);
        for(Integer i : passengers) {
        	
        	insert("INSERT INTO removed_from_rides (user_id, ride_id) VALUES (?, ?);", i, rideId);
        }
        
        insert("INSERT INTO removed_from_rides (user_id, ride_id) VALUES (?, ?);", r.getDriverId(), rideId);
        
        return execute("DELETE FROM ride WHERE ride_id = ?", rideId) > 0;
    }

    /**
     * Delete a ride as a driver by the drivers userId
     *
     * @param rideId ride to remove
     * @param userId user connected to ride
     * @return true if the ride was deleted, false otherwise.
     */
    public boolean deleteRideAsDriver(int rideId, int userId) {
        Ride r = getRideFromRideId(rideId);
        if(isCompleted(rideId, r.getLatestArrivalTime()) || isFull(rideId) || isLocked(rideId, r.getLatestArrivalTime(), r.getStartCity(), r.getEndCity())) {
            return false;
        }
        DataAccess<Integer> counter = new DataAccess<>(getDriverUrl(), (rs) -> rs.getInt(1));
        
        List<Integer> passengers = counter.query("SELECT user_id FROM passenger WHERE ride_id = ?", rideId);
        for(Integer i : passengers) {
        	insert("INSERT into removed_from_rides (user_id, ride_id) VALUES (?, ?)", i, rideId);
        }
        return execute("DELETE FROM ride WHERE ride_id = ? AND driver_id = ?", rideId, userId) > 0;
    }

    /**
     * Check for all rides which has been removed since last log in which the user was a part of.
     *
     * @param userId, the is of the user.
     * @return a list of id's of removed rides in which the user was a part of before they were removed.
     * @throws DataAccessException if the user could'nt be found.
     * */
    public List<Integer> checkForRemovedRides(int userId){
        DataAccess<Integer> counter = new DataAccess<>(getDriverUrl(), (rs) -> rs.getInt(1));
        List<Integer> ret = counter.query("SELECT ride_id FROM removed_from_rides WHERE user_id = ?",userId);
        execute("DELETE FROM removed_from_rides WHERE user_id = ?", userId);
        return ret;
    }

    /**
     * Get all rides created by a user.
     *
     * @param userId user id to filter on.
     * @return users ride objects.
     * @throws DataAccessException if the rides can't be found.
     */
    public List<Ride> getRidesWhereDriver(int userId) {
        return query("SELECT * FROM ride WHERE driver_id = ?", userId);
    }

    /**
     * Get all completed rides where user is passenger or driver.
     *
     * @param userId user id to filter on.
     * @return completed ride objects.
     * @throws DataAccessException if the rides can't be found
     */
    public List<Ride> getUsersCompletedRides(int userId) {
        List<Ride> rides = query("SELECT * FROM ride WHERE driver_id = ? " +
                                      "AND latest_arrival_time < CURRENT_TIMESTAMP() " +
                                      "AND DATEDIFF('day', latest_arrival_time, CURRENT_TIMESTAMP()) <= 7", userId);

        rides.addAll(query("SELECT ride.* FROM passenger " +
                                "INNER JOIN ride ON passenger.ride_id = ride.ride_id " +
                                "WHERE passenger.user_id = ?", userId));
        rides.removeIf(r -> !r.isCompleted());
        return rides;
    }

    /**
     * Get all rides where user is passenger.
     *
     * @param userId id of the user.
     * @return list of rides where user is a passenger.
     */
    public List<Ride> getRidesWherePassenger(int userId) {
        return query("SELECT ride.* FROM passenger " +
                          "INNER JOIN ride ON passenger.ride_id = ride.ride_id " +
                          "WHERE passenger.user_id = ?", userId);
    }

    /**
     * Get all rides.
     *
     * @return all rides
     * @throws DataAccessException if the rides can't be found
     */
    public List<Ride> getAllRides() {
        return query("SELECT * FROM ride");
    }

    /**
     * Get the 10 best matching rides.
     * @param startCity the starting city
     * @param endCity the ending city
     * @param latestArrivalTime the latest time that the user can arrive to the destination
     * @return list of the best matching rides
     * @throws DataAccessException if the rides can't be found
     */
    public List<Ride> getMatchingRides(String startCity, String endCity, long latestArrivalTime) {
        List<Ride> matchingRides = query("SELECT * FROM ride WHERE start_city = ? AND end_city = ? " +
        								 "AND latest_arrival_time < ? ;", startCity, endCity, new Timestamp(latestArrivalTime).toString());

        return matchingRides.stream()
                .filter(r -> !isCompleted(r.getId(),r.getLatestArrivalTime()))
                .filter(r -> !isLocked(r.getId(),r.getLatestArrivalTime(),r.getStartCity(),r.getEndCity()))
                .filter(r -> !isFull(r.getId()))
                .sorted(new SortRides(latestArrivalTime))
                .limit(10)
                .collect(Collectors.toList());
    }

    /**
     * Checks if a ride is locked.
     *
     * @param rideId of the ride.
     * @return true if locked, else false.
     * @throws DataAccessException if the status can't be checked
     */
    public boolean isLocked(int rideId, long latestArrivalTime, String startCity, String endCity) {
        if(isCompleted(rideId, latestArrivalTime)){
            return false;
        }
        long rideTotalTime = getTotalDuration(rideId, startCity, endCity);
        long now = System.currentTimeMillis();
        long oneHour = 3600000;                                             // in ms

        return now > (latestArrivalTime - rideTotalTime - oneHour);
    }

    /**
     * Checks if a ride is full.
     *
     * @param rideId of the ride.
     * @return true if full, else false.
     * @throws DataAccessException if the status can't be checked
     */
    public boolean isFull(int rideId) {
        DataAccess<Integer> counter = new DataAccess<>(getDriverUrl(), (rs) -> rs.getInt(1));
        int nbrOfPassengers = counter.queryFirst("SELECT COUNT(*) FROM passenger WHERE ride_id = ?", rideId);
        int capacity = counter.queryFirst("SELECT available_capacity FROM ride WHERE ride_id = ?", rideId);

        return nbrOfPassengers >= capacity;
    }

    /**
     * Checks if a ride has been completed.
     * A ride is defined as completed if latest arrival time has passed.
     *
     * @param rideId of said ride
     * @return true if completed, else false.
     * @throws DataAccessException if the status can't be checked
     */
    public boolean isCompleted(int rideId, long latestArrivalTime) {
        return latestArrivalTime < System.currentTimeMillis();
    }

    /**
     * Calculates the time necessary to drive between two cities with delays.
     *
     * @param rideId of said ride
     * @return time in ms.
     * @throws DataAccessException if the duration can't be checked
     */
    public long getTotalDuration(int rideId, String startCity, String endCity) {
        DataAccess<Long> counter = new DataAccess<>(getDriverUrl(), (rs) -> rs.getLong(1));

        long nbrOfPassengers = counter.queryFirst("SELECT COUNT(*) FROM passenger WHERE ride_id = ?", rideId);
        City start = cityDao.queryFirst("SELECT * FROM city WHERE location_name = ?", startCity);
        City end = cityDao.queryFirst("SELECT * FROM city WHERE location_name = ?", endCity);

        return 1000 * 60 * nbrOfPassengers * (start.getStopDelay() + end.getStopDelay()) + timeBetween(start, end);
    }

    /**
     * Calculates time (in ms) between two cities without delays.
     *
     * @param start, the city to drive from
     * @param end, the city to drive to
     * @return the time it takes to drive between the cities.
     * */
    public long timeBetween(City start, City end) {
        double startLatitude = (double) start.getLatitude();
        double startLongitude = (double) start.getLongitude();
        double endLatitude = (double) end.getLatitude();
        double endLongitude = (double) end.getLongitude();

        double avg_speed = 65;                                              // km per hour
        double distance = haversine(startLatitude, startLongitude, endLatitude, endLongitude);
        return 3600 * 1000 * (long) (distance / avg_speed);
    }

    /**
     * Calculates distance between (lat1, lon1) and (lat2, lon2) in km. Using the haversine formula.
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return distance in km.
     * */
    private static double haversine(double lat1, double lon1, double lat2, double lon2) {
        double R = 6372.8;                                              // in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    /**
     * Get ride with given ride id.
     *
     * @param rideId the id of the ride.
     * @return the ride.
     * @throws DataAccessException if the ride can't be found.
     */
    public Ride getRideFromRideId(int rideId) {
        return queryFirst("SELECT * FROM ride WHERE ride_id = ?", rideId);
    }

}
