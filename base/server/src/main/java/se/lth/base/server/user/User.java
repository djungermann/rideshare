package se.lth.base.server.user;

import java.security.Principal;

public class User implements Principal {

    public static User NONE = new User(0, Role.NONE, "-", "-", "-",  false);

    private final int id;
    private final Role role;
    private final String username;
    private String displayName;
    private String phoneNbr;
    private boolean removed;

    public User(int id, Role role, String username, String displayName, String phoneNbr, boolean removed) {
        this.id = id;
        this.role = role;
        this.username = username;
        this.displayName = displayName;
        this.phoneNbr = phoneNbr;
        this.removed = removed;
    }

    public Role getRole() {
        return role;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPhoneNbr() {
        return phoneNbr;
    }

    public boolean isRemoved() {
        return removed;
    }

}
