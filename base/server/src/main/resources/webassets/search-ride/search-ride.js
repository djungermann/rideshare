var base = base || {};

base.searchRideController = function() {
    'use strict' // add this to avoid some potential bugs

    let cityModel = [];

    const CityViewModel = function (_city) {

		this.city = _city;

		const viewModel = this;

        this.render = function(template) {
            this.update(template.content.querySelector('option'));
            const clone = document.importNode(template.content, true);
            template.parentElement.appendChild(clone);
        };

		this.update = function(option) {
			option.textContent = viewModel.city["locationName"];
        };
	};
	
	const MatchingRidesViewModel = function(_ride) {

		this.ride = _ride;

		const viewModel = this;

		this.render = function (template) {
			this.update(template);
			const clone = document.importNode(template.content, true);

			clone.querySelector("button").onclick = function (event) {
				controller.joinRide(viewModel.ride.id);
				return false;
			};

			template.parentElement.appendChild(clone);
		};

		this.update = function(template) {
			const d = new Date(viewModel.ride.latestArrivalTime);
			template.content.querySelectorAll("dd")[0].textContent = d.toLocaleString();
			//template.content.querySelectorAll("dd")[1].textContent = viewModel.ride.pickupTime;
		};
	};

    const view = {
        render: function() {
            const tStart = this.templateStart();
			cityModel.forEach(d => d.render(tStart));
			
			const tEnd = this.templateEnd();
			cityModel.forEach(d => d.render(tEnd));
        },

        templateStart: function() {
            return document.getElementById('start-city-template');
		},
		
		templateEnd: function () {
			return document.getElementById('end-city-template');
		},

		templateMatching: function () {
			return document.getElementById('matching-ride');
		},

		removeChildren: function (template) {
			const children = [...template.parentElement.children];
			for (const child of children) {
				if (child.tagName !== 'TEMPLATE') {
					child.remove();
				}
			}
		}
    };

    const controller = {
        load: function() {
            document.getElementById('search-ride-form').onsubmit = function (event) {
                event.preventDefault();
                controller.searchRide();
                return false;
            };

			base.rest.getAllCities().then(function (cities) {
                cityModel = cities.map(f => new CityViewModel(f));
                view.render();
            });
        },

		searchRide: function () {
			const startCity = document.getElementById('startCity').value;			
			const endCity = document.getElementById('dropOffCity').value;

			const latestArrivalTime = document.getElementById('latestArrivalTime').value;
			const timestamp = new Date(latestArrivalTime).getTime();
			
			if (startCity === endCity) {
				alert("The pickup city must differ from the drop off city");
				return;
			}

			if (typeof timestamp !== 'number') {
				alert("Bad arrival time.");
				return;
			}

			if (timestamp <= Date.now()) {
				alert("Please enter a later dropoff time.");
				return;
			}

			base.rest.findRide({
				startCity,
				endCity,
				latestArrivalTime: timestamp
			}).then(function(rides) {
				const tMatching = view.templateMatching();
				view.removeChildren(tMatching);
	
				for (const ride of rides) {
					const vm = new MatchingRidesViewModel(ride);
					vm.render(tMatching);
				}
				
				if (!rides || rides.length < 1) {
					alert("Sorry we found no rides that matched. Please try again later.");
				}
			});
		},
		
		joinRide: function (rideId) {
			const startAddress = document.getElementById('startAddress').value;
			const endAddress = document.getElementById('dropOffAddress').value;
			if (!startAddress || !endAddress) {
				alert("Plese enter a start and dropoff address.");
				return;
			}

			base.rest.joinRide({
				rideId: rideId,
				pickUpAddress: startAddress,
				dropOffAddress: endAddress
			}).then(() => {
				alert("Successfully joined ride!");
			})
		}
    };

    return controller;
};

