var base = base || {};

base.myRidesController = function () {
	'use strict'

	let upcomingRidesModel = [];
	let pastRidesModel = [];

	const RideViewModel = function (_ride) {
		this.ride = _ride;

		const viewModel = this;

		this.render = function (template) {			
			const clone = document.importNode(template.content, true);

			this.updateRideInfo([...clone.querySelectorAll(".ride-info dd")]);

			this.updateButtons(clone);
			
			const passengerTemplate = clone.getElementById('passenger-template');
			for (const passenger of viewModel.ride.passengers) {
				this.addPassenger(passengerTemplate, passenger);
			}
			
			template.parentElement.appendChild(clone);
		};

		this.updateRideInfo = function(dd) {		
			let dropoff = (new Date(viewModel.ride.latestArrivalTime)).toLocaleString();

			if (!viewModel.ride.isDriver) {
				const p = viewModel.ride.passengers.find(x => x.isSelf);
				if (p) {
					const pickup = (new Date(p.pickup)).toLocaleString();
					dropoff = (new Date(p.dropoff)).toLocaleString();

					const pickupDT = document.createElement('dt');
					const pickupDD = document.createElement('dd');

					pickupDT.textContent = "Pickup Time";
					pickupDD.textContent = pickup;

					dd[0].parentElement.prepend(pickupDD);
					dd[0].parentElement.prepend(pickupDT);
				}
			}

			dd[0].textContent = dropoff;
			dd[1].textContent = viewModel.ride.startCity + " → " + viewModel.ride.endCity;
			dd[2].textContent = viewModel.ride.car;
			dd[3].textContent = viewModel.ride.locked ? "Yup" : "Nope";
			dd[4].textContent = viewModel.ride.full ? "Yup" : "Nope";
		};

		this.updateButtons = function(clone) {
			const buttons = clone.querySelectorAll("button");

			if (!buttons || buttons.length !== 2) {
				return;
			}

			if (viewModel.ride.isDriver) {
				// update id
				const newId = "passengers-" + Math.round(Math.random() * 1000000);
				buttons[0].dataset.target = "#" + newId;
				clone.querySelector("#passengers").id = newId;

				buttons[1].onclick = function (event) {
					controller.cancelRide(viewModel.ride.id);
					return false;
				};
			} else {
				buttons[1].textContent = 'Leave Ride';
				buttons[1].onclick = function (event) {
					controller.leaveRide(viewModel.ride.id);
					return false;
				};

				buttons[0].parentNode.removeChild(buttons[0]);
			}
		}

		this.addPassenger = function (template, passenger) {
			if (!template) {
				return;
			}
			const clone = document.importNode(template.content, true);
			const dd = clone.querySelectorAll(".passenger-info dd");

			dd[0].textContent = passenger.displayName;
			dd[1].textContent = passenger.phoneNbr;

			dd[3].textContent = passenger.pickUpAddress;
			dd[5].textContent = passenger.dropOffAddress;

			dd[2].textContent = (new Date(passenger.pickup)).toLocaleString();
			dd[4].textContent = (new Date(passenger.dropoff)).toLocaleString();

			template.parentElement.appendChild(clone);
		}
	};

	const view = {
		render: function () {
			const upcomingRidesTemplate = this.upcomingTemplate();
			this.removeChildren(upcomingRidesTemplate);
			upcomingRidesModel.forEach(d => d.render(upcomingRidesTemplate));

			const pastRidesTemplate = this.pastTemplate();
			this.removeChildren(pastRidesTemplate);
			pastRidesModel.forEach(d => d.render(pastRidesTemplate));
		},

		upcomingTemplate: function () {
			return document.getElementById('ride-template');
		},

		pastTemplate: function () {
			return document.getElementById('past-ride-template');
		},

		removeChildren: function (template) {
			const children = [...template.parentElement.children];
			for (const child of children) {
				if (child.tagName !== 'TEMPLATE') {
					child.remove();
				}
			}
		},
	};

	const controller = {
		load: async function () {
			const [ completed, upcoming, user ]= await Promise.all([
				base.rest.getCompletedRides(),
				base.rest.getUpcomingRides(),
				base.rest.getUser(),
			]);


			for (const u of upcoming) {
				const [ passengers, car] = await Promise.all([
					base.rest.getPassengers(u.id),
					base.rest.getCarByRideId(u.id)
				]);
				
				await Promise.all(passengers.map(async p => {
					const passengerInfo = await base.rest.getOtherUser(p.userId);
					Object.assign(p, passengerInfo);

					// TODO
					p.pickup = 0;
					p.dropoff = 0;

					p.isSelf = (p.userId == user.id);

					return p;
				}));

				u.passengers = passengers || [];
				u.car = `${car.regNbr} | ${car.color} | ${car.model}`;

				u.isDriver = (u.driverId === user.id);
			}
			
			for (const c of completed) {
				const car = await base.rest.getCarByRideId(c.id);
				c.car = `${car.regNbr} | ${car.color} | ${car.model}`;
				c.passengers = [];
			}

			upcomingRidesModel = upcoming.map(u => new RideViewModel(u));
			pastRidesModel = completed.map(c => new RideViewModel(c));

			view.render();
		},


		cancelRide: function (rideId) {
			console.log("Should cancel ride with id: " + rideId);
			base.rest.deleteRide(rideId).then(this.load);
		},

		leaveRide: function (rideId) {
			console.log("Should leave ride with id:", rideId);
			base.rest.leaveRide(rideId).then(this.load);
		}
	};

	return controller;
};